package no.gangang.assignment.controllers;

import no.gangang.assignment.data_access.Customer.CustomerRepository;
import no.gangang.assignment.models.Customer.Customer;
import no.gangang.assignment.models.Customer.CustomerCountry;
import no.gangang.assignment.models.Customer.CustomerGenre;
import no.gangang.assignment.models.Customer.CustomerSpender;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/test")
    public String test() {
        return "OK TEST TWO";
    }

    /**
     * Get all customers in the database
     * @return ArrayList with all customers
     */
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    /**
     * Get a specific customer by id
     * @param id customer id
     * @return Customer object
     */
    @RequestMapping(value = "api/customer/{id}", method = RequestMethod.GET)
    public Customer getCustomerByPathId(@PathVariable String id){
        return customerRepository.getCustomerById(id);
    }

    /**
     * Search for a customer by name
     * @param name name of the customer
     * @return Customer object
     */
    @RequestMapping(value="/api/customers/search", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomerByName(@RequestParam String name) {
        return customerRepository.getCustomerByName(name);
    }

    /**
     * Get a specified number of customers with specified offset
     * @param limit how many customers to get
     * @param offset offset amount
     * @return page of customers
     */
    @RequestMapping(value="/api/customers/limit/{limit}/offset/{offset}", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomers(@PathVariable String limit, @PathVariable String offset) {
        return customerRepository.getCustomers(limit, offset);
    }

    /**
     * Get the highest spending customers by total invoice value. Sorted descending.
     * @return Array of the highest spending customers with total amount.
     */
    @RequestMapping(value="/api/customers/spenders", method = RequestMethod.GET)
    public ArrayList<CustomerSpender> getSpenderCostumers() {
        return customerRepository.getSpenderCustomers();
    }

    /**
     * Get the number of customers in each country
     * @return ArrayList of countries with number of customers
     */
    @RequestMapping(value="/api/countries", method = RequestMethod.GET)
    public ArrayList<CustomerCountry> getNumberOfCustomersInCountry() {
        return customerRepository.getNumberOfCustomersInCountry();
    }

    /**
     * Get the most popular genre(s) for a given customer
     * @param id customer id
     * @return Customer, genre, tracks in that genre
     */
    @RequestMapping(value = "api/customer/genres/{id}", method = RequestMethod.GET)
    public CustomerGenre getCustomerMostPopularGenre(@PathVariable String id){
        return customerRepository.getMostPopularGenre(id);
    }

    /**
     * Create a new customer
     * @param c customer object to create
     * @return true if success, false if fail
     */
    @RequestMapping(value="api/customer/new", method = RequestMethod.POST)
    public boolean addCustomer(@RequestBody Customer c) {
        return customerRepository.addCustomer(c);
    }

    /**
     * Update a customer with given parameters
     * @param c customer object with variables to update
     * @return true if success, false if fail
     */
    @RequestMapping(value="api/customer/update", method = RequestMethod.PUT)
    public boolean updateCustomer(@RequestBody Customer c) {
        return customerRepository.updateCustomer(c);
    }
}
