package no.gangang.assignment.controllers;

import no.gangang.assignment.data_access.Media.MediaRepository;
import no.gangang.assignment.models.Media.Genre;
import no.gangang.assignment.models.Media.Song;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;

@Controller
public class MediaController {

    private final MediaRepository mediaRepository;

    public MediaController(MediaRepository mediaRepository) {
        this.mediaRepository = mediaRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getRandomArtists(Model model) {
        model.addAttribute("artists", mediaRepository.getRandomArtists());
        model.addAttribute("songs", mediaRepository.getRandomSongs());
        model.addAttribute("genres", mediaRepository.getRandomGenres());
        return "index";
    }

    @RequestMapping(value = "/media/songs", method = RequestMethod.GET)
    public ArrayList<Song> getRandomSongs(Model model) {
        model.addAttribute("songs", mediaRepository.getRandomSongs());
        return mediaRepository.getRandomSongs();
    }

    @RequestMapping(value = "/media/genres", method = RequestMethod.GET)
    public ArrayList<Genre> getRandomGenres(Model model) {
        model.addAttribute("genres", mediaRepository.getRandomGenres());
        return mediaRepository.getRandomGenres();
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String getCustomerByName(String query, Model model) {
        ArrayList<Song> res = mediaRepository.searchForSongs(query);
        model.addAttribute("results", res);
        return "view-search";
    }
}

