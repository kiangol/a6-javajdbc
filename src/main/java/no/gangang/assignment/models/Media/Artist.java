package no.gangang.assignment.models.Media;

public class Artist {
    private String artistId;
    private String Name;

    public Artist(String artistId, String name) {
        this.artistId = artistId;
        Name = name;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
