package no.gangang.assignment.models.Media;

public class Song {
    private String songId;
    private String songName;
    private String artistName;

    public Song(String songId, String songName, String artistName) {
        this.songId = songId;
        this.songName = songName;
        this.artistName = artistName;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}
