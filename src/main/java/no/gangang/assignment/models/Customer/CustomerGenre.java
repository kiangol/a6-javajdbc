package no.gangang.assignment.models.Customer;

import java.util.ArrayList;

public class CustomerGenre {
    private String customerId;
    private ArrayList<String> mostPopularGenres;
    private int genreCount;

    public CustomerGenre(String customerId) {
        this.customerId = customerId;
        this.genreCount = 0;
        mostPopularGenres = new ArrayList<>();
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void addGenre(String genre) {
        this.mostPopularGenres.add(genre);
    }

    public int getGenreCount() {
        return genreCount;
    }

    public void setGenreCount(int genreCount) {
        this.genreCount = genreCount;
    }

    public ArrayList<String> getMostPopularGenres() {
        return mostPopularGenres;
    }

    public void setMostPopularGenres(ArrayList<String> mostPopularGenres) {
        this.mostPopularGenres = mostPopularGenres;
    }
}


