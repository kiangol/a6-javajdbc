package no.gangang.assignment.models.Customer;

public class CustomerCountry {
    private String Country;
    private int numOfCustomers;

    public CustomerCountry(String country, int numOfCustomers) {
        Country = country;
        this.numOfCustomers = numOfCustomers;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public int getNumOfCustomers() {
        return numOfCustomers;
    }

    public void setNumOfCustomers(int numOfCustomers) {
        this.numOfCustomers = numOfCustomers;
    }
}
