package no.gangang.assignment.models.Customer;

public class CustomerSpender {
    private String customerId;
    private String totalSpent;

    public CustomerSpender(String customerId, String totalSpent) {
        this.customerId = customerId;
        this.totalSpent = totalSpent;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(String totalSpent) {
        this.totalSpent = totalSpent;
    }
}
