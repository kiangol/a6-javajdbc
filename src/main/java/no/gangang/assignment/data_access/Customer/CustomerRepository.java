package no.gangang.assignment.data_access.Customer;
import no.gangang.assignment.models.Customer.Customer;
import no.gangang.assignment.models.Customer.CustomerCountry;
import no.gangang.assignment.models.Customer.CustomerGenre;
import no.gangang.assignment.models.Customer.CustomerSpender;

import java.util.ArrayList;

public interface CustomerRepository {
    ArrayList<Customer> getAllCustomers();
    Customer getCustomerById(String customerId);
    ArrayList<Customer> getCustomerByName(String name);
    ArrayList<Customer> getCustomers(String limit, String offset);
    ArrayList<CustomerCountry> getNumberOfCustomersInCountry();
    ArrayList<CustomerSpender> getSpenderCustomers();
    CustomerGenre getMostPopularGenre(String customerId);
    Boolean addCustomer(Customer customer);
    Boolean updateCustomer(Customer customerId);
}
