package no.gangang.assignment.data_access.Customer;

import no.gangang.assignment.data_access.ConnectionHelper;
import no.gangang.assignment.logging.LogToConsole;
import no.gangang.assignment.models.Customer.Customer;
import no.gangang.assignment.models.Customer.CustomerCountry;
import no.gangang.assignment.models.Customer.CustomerGenre;
import no.gangang.assignment.models.Customer.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final LogToConsole logger;

    private final String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public CustomerRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    @Override
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Customer");

            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                customers.add(
                    new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                    )
                );
            }
            logger.log("Success: select all customers");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return customers;
    }


    @Override
    public Customer getCustomerById(String customerId) {
        Customer customer = null;
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Customer WHERE CustomerId = ?");
            stmt.setString(1, customerId);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                    resultSet.getString("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
                );
            }
            logger.log("Success: select specific customers");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return customer;
    }

    @Override
    public ArrayList<Customer> getCustomerByName(String name) {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Customer WHERE FirstName LIKE ?");

            stmt.setString(1, "%" + name + "%");

            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                System.out.println(resultSet.getString("CustomerId"));
                customers.add(
                    new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                    )
                );
            }
            logger.log("Success: select customer by name");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return customers;
    }

    @Override
    public ArrayList<Customer> getCustomers(String limit, String offset) {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Customer ORDER BY CustomerId LIMIT ? OFFSET ?");

            stmt.setString(1, limit);
            stmt.setString(2, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                customers.add(
                    new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                    )
                );
            }
            logger.log("Success: select limited number of customers");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return customers;
    }

    @Override
    public ArrayList<CustomerCountry> getNumberOfCustomersInCountry() {
        ArrayList<CustomerCountry> countries = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("SELECT country, COUNT (country) AS numOfCustomers from Customer GROUP BY country ORDER BY numOfCustomers DESC");
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                countries.add(
                    new CustomerCountry(
                        resultSet.getString("Country"),
                        resultSet.getInt("numOfCustomers")
                    )
                );
            }

            logger.log("Success: get number of customers in each country");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return countries;
    }

    @Override
    public ArrayList<CustomerSpender> getSpenderCustomers() {
        ArrayList<CustomerSpender> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("SELECT customerid, SUM(total) as TotalSpent FROM Invoice GROUP BY customerid ORDER BY TotalSpent DESC");


            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                customers.add(
                    new CustomerSpender(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("TotalSpent")
                    )
                );
            }
            logger.log("Success: select highest spenders of customers");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return customers;
    }

    @Override
    public CustomerGenre getMostPopularGenre(String customerId) {
        CustomerGenre customer = new CustomerGenre(customerId);
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("SELECT Genre.genreid, Genre.name, COUNT(*) AS num FROM Customer " +
                    "INNER JOIN Invoice on Customer.customerid = Invoice.customerid " +
                    "INNER JOIN InvoiceLine ON InvoiceLine.invoiceid = Invoice.invoiceid " +
                    "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                    "INNER JOIN Genre ON Track.genreid = Genre.GenreId " +
                    "WHERE Customer.CustomerId = ? GROUP BY Genre.genreid ORDER BY num DESC ");

            stmt.setString(1, customerId);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                int count = resultSet.getInt("num");
                if (count < customer.getGenreCount()) {
                    break;
                }
                customer.addGenre(resultSet.getString("Name"));
                customer.setGenreCount(count);
            }

            logger.log("Success: get the most popular genre from specific customer");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return customer;
    }

    @Override
    public Boolean addCustomer(Customer customer) {
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                conn.prepareStatement("INSERT INTO Customer (CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)");

            stmt.setString(1, customer.getCustomerId());
            stmt.setString(2, customer.getFirstName());
            stmt.setString(3, customer.getLastName());
            stmt.setString(4, customer.getCountry());
            stmt.setString(5, customer.getPostalCode());
            stmt.setString(6, customer.getPhone());
            stmt.setString(7, customer.getEmail());

            stmt.executeUpdate();

            logger.log("Success: add customer");

        } catch (Exception e) {
            logger.log(e.toString());
            return false;
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return true;
    }

    @Override
    public Boolean updateCustomer(Customer customer) {
        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            String sql = "UPDATE Customer SET ";
            BeanInfo beanInfo = Introspector.getBeanInfo(Customer.class);

            for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
                String propertyName = propertyDesc.getDisplayName();
                Object value = propertyDesc.getReadMethod().invoke(customer);
                if (value != null && !propertyName.equals("class") && !propertyName.equals("customerId")) {
                    sql += propertyName + " = ?, ";
                }
            }
            sql = sql.substring(0, sql.length() - 2);

            sql += " WHERE customerId = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            int counter = 1;
            String customerId = "";
            for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
                String propertyName = propertyDesc.getDisplayName();
                Object value = propertyDesc.getReadMethod().invoke(customer);
                if (value != null && !propertyName.equals("class") && !propertyName.equals("customerId")) {
                    stmt.setString(counter, value.toString());
                    counter++;
                } else if (propertyName.equals("customerId")) {
                    customerId = value.toString();
                }
            }

            stmt.setString(counter, customerId);

            stmt.executeUpdate();

            logger.log("Success: updated customer");

        } catch (Exception e) {
            logger.log(e.toString());
            return false;
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return true;
    }
}
