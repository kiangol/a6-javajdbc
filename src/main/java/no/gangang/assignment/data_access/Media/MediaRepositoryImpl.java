package no.gangang.assignment.data_access.Media;

import no.gangang.assignment.data_access.ConnectionHelper;
import no.gangang.assignment.logging.LogToConsole;
import no.gangang.assignment.models.Customer.Customer;
import no.gangang.assignment.models.Media.Artist;
import no.gangang.assignment.models.Media.Genre;
import no.gangang.assignment.models.Media.Song;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Repository
public class MediaRepositoryImpl implements MediaRepository {
    private final LogToConsole logger;

    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public MediaRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    @Override
    public ArrayList<Artist> getRandomArtists() {
        ArrayList<Artist> artists = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                    conn.prepareStatement("SELECT artistId,name FROM Artist ORDER BY RANDOM() LIMIT 6");


            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                artists.add(
                        new Artist(
                                resultSet.getString("artistId"),
                                resultSet.getString("name")
                        )
                );
            }
            logger.log("Success: select five random artist");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return artists;
    }

    @Override
    public ArrayList<Song> getRandomSongs() {
        ArrayList<Song> songs = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                    conn.prepareStatement("SELECT trackId, Track.Name as songName, Artist.Name as artistName FROM Track " +
                        "INNER JOIN Album ON Track.albumid = Album.AlbumId " +
                        "INNER JOIN Artist ON Album.albumid = Artist.artistid " +
                        " ORDER BY RANDOM() LIMIT 6");


            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                songs.add(
                        new Song(
                                resultSet.getString("trackId"),
                                resultSet.getString("songName"),
                                resultSet.getString("artistName")
                        )
                );
            }
            logger.log("Success: select five random artist");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return songs;
    }

    @Override
    public ArrayList<Genre> getRandomGenres() {
        ArrayList<Genre> genres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                    conn.prepareStatement("SELECT genreId,name FROM Genre ORDER BY RANDOM() LIMIT 6");


            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                genres.add(
                        new Genre(
                                resultSet.getString("genreId"),
                                resultSet.getString("name")
                        )
                );
            }
            logger.log("Success: select five random artist");
        } catch (Exception e) {
            logger.log(e.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return genres;
    }

    @Override
    public ArrayList<Song> searchForSongs(String name) {
        ArrayList<Song> songs = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            PreparedStatement stmt =
                    conn.prepareStatement("SELECT Artist.name AS artistName, TrackId, Track.name AS songName from Track " +
                            "INNER JOIN Album ON Track.albumid = Album.AlbumId " +
                            "INNER JOIN Artist ON Album.albumid = Artist.artistid " +
                            "WHERE Track.name LIKE ? OR Artist.Name LIKE ?");

            stmt.setString(1, "%" + name + "%");
            stmt.setString(2, "%" + name + "%");

            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                songs.add(
                        new Song(
                                resultSet.getString("trackId"),
                                resultSet.getString("songName"),
                                resultSet.getString("artistName")
                        )
                );
            }
            logger.log("Success: search for song");
        } catch (Exception e) {
            logger.log(e.toString());
        }

        finally {
            try {
                conn.close();
            } catch (Exception e) {
                logger.log(e.toString());
            }
        }
        return songs;
    }


}
