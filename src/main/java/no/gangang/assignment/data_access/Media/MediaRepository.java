package no.gangang.assignment.data_access.Media;

import no.gangang.assignment.models.Customer.Customer;
import no.gangang.assignment.models.Media.Artist;
import no.gangang.assignment.models.Media.Genre;
import no.gangang.assignment.models.Media.Song;

import java.util.ArrayList;

public interface MediaRepository {
    ArrayList<Artist> getRandomArtists();
    ArrayList<Song> getRandomSongs();
    ArrayList<Genre> getRandomGenres();
    ArrayList<Song> searchForSongs(String name);


}
