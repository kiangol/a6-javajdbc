# REST API - Customers

Running on <https://a6-emil-kian.herokuapp.com>

## Get all customers

Read all customers in the database. Displays their ID, first name, last name, country, postal code, phone number and
email.

### Request

`GET /api/customers/`

Example:

`curl -i -H 'Accept: application/json' https://a6-emil-kian.herokuapp.com/api/customers/`

### Response

```json
[
  {
    "customerId": "1",
    "firstName": "Luís",
    "lastName": "Gonçalves",
    "country": "Brazil",
    "postalCode": "12227-000",
    "phone": "+55 (12) 3923-5555",
    "email": "luisg@embraer.com.br"
  },
  {},
  {}
]
```

## Read a specific customer by ID

Read a specific customer from the database by ID.

### Request

`GET /api/customer/{id}`

Example:

`curl -i -H 'Accept: application/json' https://a6-emil-kian.herokuapp.com/api/customer/1`

### Response

```json
[
  {
    "customerId": "1",
    "firstName": "Luís",
    "lastName": "Gonçalves",
    "country": "Brazil",
    "postalCode": "12227-000",
    "phone": "+55 (12) 3923-5555",
    "email": "luisg@embraer.com.br"
  }
]
```

## Read a specific customer by name

Read a specific customer from the database by name.

### Request

`GET /api/customers/search?name={query}`

Example:

`curl -i -H 'Accept: application/json' https://a6-emil-kian.herokuapp.com/api/customers/search?name=luis`

### Response

```json
[
  {
    "customerId": "57",
    "firstName": "Luis",
    "lastName": "Rojas",
    "country": "Chile",
    "postalCode": null,
    "phone": "+56 (0)2 635 4444",
    "email": "luisrojas@yahoo.cl"
  }
]
```

## Get a page of customers

Get a specified amount of customers, with specified offset

### Request

`GET /api/customers/limit/{limit}/offset/{offset}`

Example:

`curl -i -H 'Accept: application/json' https://a6-emil-kian.herokuapp.com/api/customers/limit/2/offset/5`

### Response

```json
[
  {
    "customerId": "6",
    "firstName": "Helena",
    "lastName": "Holý",
    "country": "Czech Republic",
    "postalCode": "14300",
    "phone": "+420 2 4177 0449",
    "email": "hholy@gmail.com"
  },
  {
    "customerId": "7",
    "firstName": "Astrid",
    "lastName": "Gruber",
    "country": "Austria",
    "postalCode": "1010",
    "phone": "+43 01 5134505",
    "email": "astrid.gruber@apple.at"
  }
]
```

## Add a new customer

Add a new customer to the database, with data in the request body. Returns `true` if successful, else `false`.

### Request

`POST /api/customer/new`

Example:
`POST https://a6-emil-kian.herokuapp.com/api/customer/new`
Body:

```json
{
  "firstName": "Kian",
  "lastName": "Dogg",
  "country": "Bahamas",
  "postalCode": "1312",
  "phone": "+420 2 41 717",
  "email": "kiantheman@mail.com"
}
```

### Response

`true`

## Update an existing customer

Updates specified fields of an existing customer. Returns `true` if successful, else `false`.

### Request

`PUT /api/customer/update`

Example:
`PUT https://a6-emil-kian.herokuapp.com/api/customer/update`
Body:

```json
{
  "customerId": "63",
  "firstName": "Kian"
}
```

### Response

`true`

## Number of customers in each country

Get how many customers each country has, ordered descending (high to low).

### Request

`GET /api/countries`

Example:
`GET https://a6-emil-kian.herokuapp.com/api/countries`

### Response

```json
[
  {
    "numOfCustomers": 13,
    "country": "USA"
  },
  {
    "numOfCustomers": 8,
    "country": "Canada"
  },
  {
    "numOfCustomers": 5,
    "country": "France"
  }
]
```

## Highest spending customers
Get a descending list of the highest spending customers (total in invoice table).

### Request

`GET /api/customers/spenders`

Example:
`GET https://a6-emil-kian.herokuapp.com/api/customers/spenders`

### Response

```json
[
  {
    "customerId": "6",
    "totalSpent": "49.62"
  },
  {
    "customerId": "26",
    "totalSpent": "47.62"
  }
]
```

## Customers most popular genre
Get the most popular genre for a given customer. Displays how many tracks the customer has in that genre. 
If two genres have equal amount of tracks, both are shown.
### Request

`GET /api/customer/genres/{id}`

Example:
`GET https://a6-emil-kian.herokuapp.com/api/customer/genres/2`

### Response

```json
{
  "customerId": "2",
  "mostPopularGenres": [
    "Rock"
  ],
  "genreCount": 17
}
```

