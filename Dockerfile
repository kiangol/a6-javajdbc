FROM openjdk:16
ADD target/assignment-0.0.1-SNAPSHOT.jar a6-java.jar
ENTRYPOINT ["java", "-jar", "a6-java.jar"]
